require 'test_helper'

class ChecksheetControllerTest < ActionDispatch::IntegrationTest
  test "should get start" do
    get checksheet_start_url
    assert_response :success
  end

  test "should get result" do
    get checksheet_result_url
    assert_response :success
  end

end
