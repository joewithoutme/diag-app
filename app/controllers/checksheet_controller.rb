class ChecksheetController < ApplicationController
  def start
    @cs = CheckSheet.find_by(permalink: params[:permalink])
    @answer_list = Settings.answer_type[@cs.answer_type]
  end

  def result
    @cs = CheckSheet.find_by(permalink: params[:permalink])
    @answer = params[:answer]

  end
end
