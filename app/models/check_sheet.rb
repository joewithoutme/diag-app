class CheckSheet < ApplicationRecord
  has_many :questions, dependent: :destroy
  validates :permalink, presence: true, uniqueness: true
  validates :answer_type, presence: true

  # def to_param
  #   permalink
  # end
end