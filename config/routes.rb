Rails.application.routes.draw do

  devise_for :admin_user
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  get '/checksheet/:permalink/' => 'checksheet#start'
  post '/checksheet/:permalink/result' => 'checksheet#result'

  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
