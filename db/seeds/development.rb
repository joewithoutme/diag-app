# coding: utf-8

# AdminUsers
AdminUser.create(email: "dev@2yan.co", password: "hogehoge", password_confirmation: "hogehoge")

cs = CheckSheet.create(permalink: "baka", title: "バカ診断", answer_type: "B", description: "テスト用")
cs.questions.create(content: "間違えて携帯電話をゴミ箱に捨てたことがありますか", ordering: 0, score: "0,1,2,3")
cs.questions.create(content: "「お前はバカだ」と言われたことがありますか", ordering: 2, score: "0,1,2,3")
cs.questions.create(content: "自分はバカではないと思いますか", ordering: 1, score: "0,1,2,3")
cs.questions.create(content: "1+1=2", ordering: 2, score: "3,2,1,0")