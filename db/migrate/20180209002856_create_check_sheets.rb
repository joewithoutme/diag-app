class CreateCheckSheets < ActiveRecord::Migration[5.0]
  def change
    create_table :check_sheets do |t|
      t.string :permalink
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
