class AddColumnToCheckSheet < ActiveRecord::Migration[5.0]
  def change
    add_column :check_sheets, :answer_type, :string
  end
end
