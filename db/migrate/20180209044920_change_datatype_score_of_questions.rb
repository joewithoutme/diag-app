class ChangeDatatypeScoreOfQuestions < ActiveRecord::Migration[5.0]
  def change
    change_column :questions, :score, :string
  end
end
