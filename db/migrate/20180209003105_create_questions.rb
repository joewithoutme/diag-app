class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.integer :check_sheet_id
      t.integer :ordering
      t.string :content
      t.integer :score
      t.string :description

      t.timestamps
    end
  end
end
